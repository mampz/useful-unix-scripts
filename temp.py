import subprocess
import time
import os

max_temp=85
while True:
    proc = subprocess.Popen('sensors', stdout=subprocess.PIPE)
    output = proc.stdout.read()

    for line in output.split('\n'):
        if line.find('Core')!=-1:
            temp=float(line[line.find('+')+1:line.find('C',2)-2])

    if temp>max_temp:
        os.system('wall WARNING, CPU TEMPERATURE HIGH: '+str(temp) + 'C')

    time.sleep(5)
